var express = require('express');
var app = express();

app.use(express.static('public'));

const database = require("./server/database");

//Index html
app.get('/', function(req, res) {
    res.sendFile(__dirname + "/public/index.html");
});

function respondWithOdds(res, predicate){
    res.json(database.getAllBetsByDecimal(predicate));
}

app.get('/decimalOddsMoreThanTwo', function (req, res) {
    //code here
    respondWithOdds(res, o => o > 2);
});

app.get('/decimalOddsLessThanTwo', function (req, res) {
    //code here
    respondWithOdds(res, o => o < 2);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});