//Client side scripts.
(function () {
    "use strict";
    const container = document.getElementById('container');

    document.getElementsByName('changeOdds')
        .forEach(r => r.addEventListener(
            'change', () => {
                getOdds(r.value);
        }));
    
    getOdds('/decimalOddsMoreThanTwo');

    function getOdds(endpoint){
        container.innerHTML = '';
        fetch(endpoint)
                .then(res => res.json())
                .then(res => {
                    for (let bet of res) {
                        let bestOddsDecimal = getMax(bet.odds);
                        let div = makeBet(bet.name, bestOddsDecimal);
                        container.appendChild(div);
                    }
                });
    }
    

    function getMax(odds) {
        let bestOdds = odds.reduce((a, b) => {
            if (a.oddsDecimal > b.oddsDecimal)
                return a;
            else return b;
        });

        return bestOdds.oddsDecimal;
    }

    function makeBet(name, bestOddsDecimal) {
        let div = document.createElement('div');
        let nameSpan = document.createElement('span');
        let oddsSpan = document.createElement('span');

        nameSpan.textContent = name;
        oddsSpan.textContent = bestOddsDecimal;
        div.appendChild(nameSpan)
        div.appendChild(oddsSpan);
        div.className = "bet";
        return div;
    }
})();